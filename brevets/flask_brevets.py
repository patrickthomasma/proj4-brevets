"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from datetime import datetime as datetime
import dateutil.parser
import time
import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
logging.basicConfig(format='%(levelname)s:%(message)2',
                    level=logging.INFO)
log = logging.getLogger(__name__)
###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    log.info("he was here")
    print("I was here")
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    print("hey guys!!!")
    log.info("hey ddudes")
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    distance = request.args.get('distance', type=int)
    log.info(distance)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    date = request.args.get('seconds', type=str)
    time = request.args.get('time',type=str)
    log.info(date)
    log.info(distance)
    log.info(time)
    values = datetime.strptime(date, "%Y-%m-%d").timestamp()
    date = int(values) + int(time)
    print("This is the total", date)
    date = datetime.utcfromtimestamp(date).isoformat()
    print("The new time is:",date)
    print(type(date))
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    open_time = acp_times.open_time(km, distance, date)
    close_time = acp_times.close_time(km, distance, date)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.debug = True
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(debug=True,port=CONFIG.PORT, host="0.0.0.0")
