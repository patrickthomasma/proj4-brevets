"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow
from datetime import datetime, timedelta
import dateutil.parser
import time

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#
def open_time(km , brevet_dist, start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    d = datetime.fromisoformat(start_time)
    d = str(d)
    d2 = datetime.strptime(d, "%Y-%m-%d %H:%M:%S")
    date = time.mktime(d2.timetuple())
    if (km > brevet_dist):
        return arrow.now().isoformat()
    if (km >= 0) and (km <= 199):
        opening = km / 34
        mhours = opening * 60
        newtime = mhours *60
        times = date + newtime
    if (km >= 200) and (km<= 399):
        opening = km / 34
        mhours = opening * 60
        newtime = mhours * 60
        times = date + newtime
    if (km >= 400) and (km <= 599):
        opening = 200 / 34
        mhours = opening * 3600
        more = 200 / 32
        mores = more * 3600
        real = (km - 400) / 30
        real *= 3600
        times = mhours + mores + real + date
    if (km >= 600) and (km <= 1000):
        opening = 200 // 34
        opening *= 3600
        more = 200 // 32
        more *= 3600
        notreal = 200 // 30
        notreal *= 3600
        real = (km - 600) // 28
        real *= 3600
        times = real + more + notreal + opening + date
    newtime = datetime.fromtimestamp(times).strftime("%A, %B %d, %Y %I:%M:%S")
    print(newtime)
    return newtime


def close_time(km, brevet_dist, start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    print(start_time)
    d = datetime.fromisoformat(start_time)
    d = str(d)
    d2 = datetime.strptime(d, "%Y-%m-%d %H:%M:%S")
    date = time.mktime(d2.timetuple())
    if (km > brevet_dist):
        return arrow.now().isoformat()
    if (km >= 0) and (km <= 599):
        opening = km / 15
        mhours = opening * 60
        newtime = mhours * 60
        times = date + newtime + 60
    if (km >= 600) and (km <= 1000):
        opening = 200 // 15
        opening *= 3600
        more = 200 // 15
        more *= 3600
        notreal = 200 // 15
        notreal *= 3600
        real = (km - 600) // 11.428
        real *= 3600
        times = real + more + notreal + opening + date + 60
    newtime = datetime.fromtimestamp(times).strftime("%A, %B %d, %Y %I:%M:%S")
    print(newtime)
    return newtime
