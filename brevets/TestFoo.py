import acp_times
import nose
import arrow
from datetime import datetime,timedelta
import time

def same(s,t):
    return s == t

def test_something():
    assert same(acp_times.open_time(150,200,"2017-01-01T00:00:00"), "Sunday, January 01, 2017 04:24:42")

def test_default():
    assert same(acp_times.open_time(150,200,"2020-04-23T00:05:00"), "Thursday, April 23, 2020 04:29:42")

def test_close():
    assert same(acp_times.close_time(150,200,"2020-05-15T00:08:23"), "Friday, May 15, 2020 10:09:23")

def test_close2():
    assert same(acp_times.close_time(150,200,"2047-03-23T09:07:33"), "Saturday, March 23, 2047 07:08:33")

def test_zero():
    assert same(acp_times.open_time(150,200,"1987-02-11T05:08:24"), "Wednesday, February 11, 1987 09:33:06")

