Patrick Thomasma
pthomasm@uoregon.edu
Brevet calculator will take an input in km or mi and give a output on opening/closing times based on
the Brevet calculator algorithm for bike racing. When the user gives km that is longer than the brevet
distance then the program will give the current time instead hopefully indicating to the user that they've made an error but the program should conitnue running, an interesting glitch that
that was found for nosetesting is that when you put in nosetest in the command line the testing file will will revert the time/date back 8 hours earlier than expected (I expect this is something to do with UTC     
timezone rules) however in the Dockerfile there is a RUN nosetests TestFoo.py in it and if you run the Dockerfil then the tests will all pass and be corrrect (then -8 hours issue doesn't show up) wasn't able to find anything on the topic but its an interesting bug. For anyone wanting to test acp_times each paramater should be an int, if you put in a date for the last paramater then the testing will not work since date conversion is done on javascript for the webpage. If running nosetests on the terminal the user should make sure they are using python 3.7 as a function is used called fromisoformat which was only recently added in to python 3.7 and 3.8
